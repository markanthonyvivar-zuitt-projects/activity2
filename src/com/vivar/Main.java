package com.vivar;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your

        int year;

        Scanner appScanner = new Scanner(System.in);

        System.out.println("What is your birth year?");
         year = appScanner.nextInt();


        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    System.out.println("Leap year");
                } else {
                    System.out.println("Not a leap year");
                }
            } else {
                System.out.println("Leap year");
            }
        } else {
            System.out.println("Not a leap year");
        }



    }
}
